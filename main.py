from fastapi import FastAPI


app = FastAPI()


@app.get("/quadratic")
def read_roots(a: int, b: int, c: int):
    d = b ** 2 - 4 * a * c
    if d < 0:
        return {"Answer": "No roots"}
    if d == 0:
        root = -b / 2 * a
        return {"Root": root}
    root1 = (-b + d ** 0.5) / 2 * a
    root2 = (-b - d ** 0.5) / 2 * a
    return {"Root 1": root1, "Root 2": root2}


@app.get("/items/{item_id}/")
def read_item(item_id: int):
    return {"Expected color": "Blue"}
